Software for our home-built RC Quadcopter. 

The client-side (controller) software is written in Java using JavaFX, the server-side (drone) software is written using Java for networking and C for the hardware interface. 

The client shows the GPS positioning and otherwise controls the drone. The server operates the device and communicates with the client over the internet.

This is all still a work-in-progress!


--

Contributors:

Eli Kaplan (client, server networking)

Nes Cohen (hardware control / interfacing)