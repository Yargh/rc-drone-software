import java.io.Serializable;

/**
 * Data packet passed between server and client
 * @author Eli Kaplan
 * 
 */
public class DataPacket implements Serializable
{
	private static final long serialVersionUID = 1L;
	
	double dirDeg;
	double moveMag;
	double elevMag;
	double rotMag;
	
	public DataPacket(double dir, double move, double elev, double rot)
	{
		dirDeg = dir;
		moveMag = move;
		elevMag = elev;
		rotMag = rot;
	}
}