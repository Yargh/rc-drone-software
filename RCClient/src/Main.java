import java.io.BufferedOutputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

/**
 * RC quad client (sends commands over network)
 * @author Eli Kaplan
 *
 */
public class Main {

	
	static int serverPort = 6969; // port to listen on
	static String serverHost = "10.0.0.5"; // remote host
	
	public static void main(String[] args)
	{
		System.out.println("Starting rc client.");
		System.out.println("Connecting to " + serverHost + ":" + serverPort + ".");
		
		Socket testSocket;
		ObjectOutputStream objOut = null;
				
		try {
			
			testSocket = new Socket(serverHost, serverPort);
			objOut = new ObjectOutputStream(new BufferedOutputStream(testSocket.getOutputStream()));
			
		} catch (Exception e) {
			System.out.println("Failed to connect.");
			e.printStackTrace();
			System.exit(0);
		}
		
		System.out.println("Connected!");
		
		ControllerHandler cont = new ControllerHandler();
		cont.sendControls(objOut);
		
		System.exit(0);
	}
	
}
