/**
 * Controller handling class
 * @author Eli Kaplan
 *
 */
import java.io.ObjectOutputStream;
import java.math.BigDecimal;
import java.math.RoundingMode;

import ch.aplu.xboxcontroller.*;


public class ControllerHandler 
{
	private XboxController cont;
	private boolean isConnected = false;
	private boolean quit = false;

	
	private double moveDir = 0.0; // movement direction
	private double moveMag = 0.0; // movement magnitude
	private double elevMag = 0.0; // elevation change magnitude
	private double rotMag = 0.0; // rotation change magnitude
	
	private double lStickDZ = 0.2; // "dead zone" on left analog stick
	
	public ControllerHandler() 
	{
		System.out.println("Initializing controller.");
		System.load(System.getProperty("user.dir") + "\\lib\\xboxcontroller64.dll");
		cont = new XboxController("xboxcontroller64", 1, 50, 50);
		
		
		if(!cont.isConnected()) {
			System.out.println("No controller found!");
			cont.release();
			return;
		}
		else {
			System.out.println("Controller connected!");
			isConnected = true;
			cont.addXboxControllerListener(new InternalControllerHandler());
		}
	}
	
	public boolean isConnected()
	{
		return isConnected;
	}
	
	public double getMoveDir()
	{
		return new BigDecimal(moveDir).setScale(2, RoundingMode.HALF_UP).doubleValue(); // round to 2 places
	}
	
	public double getMoveMag()
	{
		return new BigDecimal(moveMag).setScale(2,  RoundingMode.HALF_UP).doubleValue();
	}
	
	public double getElevMag()
	{
		return new BigDecimal(elevMag).setScale(2,  RoundingMode.HALF_UP).doubleValue();
	}
	
	public double getRotMag()
	{
		return new BigDecimal(rotMag).setScale(2,  RoundingMode.HALF_UP).doubleValue();
	}
	
	
	public void sendControls(ObjectOutputStream objOut)
	{
		if(objOut != null) {
			while(!Thread.interrupted() && !quit) {
				try {
					double txMove = getMoveDir();
					double txMoveMag = getMoveMag();
					double txElevMag = getElevMag();
					double txRotMag = getRotMag();
					//System.out.print("\rTx: move = (" + txMove + "*" + txMoveMag + "), elev = " + txElevMag + ", rot = " + txElevMag + "        ");
					objOut.writeObject(new DataPacket(txMove, txMoveMag, txElevMag, txRotMag));
					objOut.flush();
					Thread.sleep(150);
				}
				catch (Exception e) {
					e.printStackTrace();
					Thread.currentThread().interrupt();
				}
			}
			
			if(quit) {
				System.exit(0);
			}
		} else {
			System.out.println("Null output stream!");
		}
	}
	
	
	/**
	 * Xbox control handler
	 */
	
	private class InternalControllerHandler extends XboxControllerAdapter
	{
		public InternalControllerHandler()
		{
			
		}
		
		
		public void rightTrigger(double value)
		{
			elevMag = value;
		}
		
		public void leftTrigger(double value)
		{
			elevMag = -value;
		}
		
		public void leftThumbDirection(double direction)
		{
			if(moveMag > lStickDZ) {
				moveDir = direction;
			} else {
				moveDir = 0.0;
			}
		}
		
		public void leftThumbMagnitude(double magnitude)
		{
			moveMag = magnitude;
		}
		
		public void leftShoulder(boolean pressed)
		{
			if(pressed) {
				rotMag = -1.0;
			} else {
				rotMag = 0.0;
			}
		}
		
		public void rightShoulder(boolean pressed)
		{
			if(pressed) {
				rotMag = 1.0;
			} else {
				rotMag = 0.0;
			}
		}
		
		public void start(boolean pressed) {
			quit = pressed;
		}
				
		public void isConnected(boolean connected)
		{
			isConnected = connected;
		}
	}
	
	
}
