package application;

import java.io.Serializable;


public class ReturnDataPacket implements Serializable {
	private static final long serialVersionUID = 1L;
	
	GPSCoord curPos;
	
	public ReturnDataPacket(GPSCoord c) {
		curPos = c;
	}
	
}
