package application;

/**
 * RC Control handler main
 * @author Eli Kaplan
 *
 */

import java.io.BufferedOutputStream;
import java.io.BufferedInputStream;
import java.io.ObjectOutputStream;
import java.io.ObjectInputStream;
import java.io.PrintStream;
import java.net.ConnectException;
import java.net.Socket;

public class RCMain extends Thread{
	
	static int serverPort = 6969; // port to listen on
	static String serverHost = "localhost"; // remote host
	
	PrintStream console;

	public RCMain(String host, int port) 
	{
		serverPort = port;
		serverHost = host;
	}
	
	public RCMain()
	{
		
	}
	
	@Override
	public void run()
	{
		while(MainController.console == null) {
			try {
				Thread.sleep(1000);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		console = MainController.console;
		
		console.println("Starting rc client.");
		console.println("Connecting to " + serverHost + ":" + serverPort + ".");
		
		Socket testSocket;
		ObjectOutputStream objOut = null;
		//ObjectInputStream objIn = null;
		
		boolean isConnected = false;
		while(!isConnected) {
			try {
				testSocket = new Socket(serverHost, serverPort);
				objOut = new ObjectOutputStream(new BufferedOutputStream(testSocket.getOutputStream()));
				//objIn = new ObjectInputStream(new BufferedInputStream(testSocket.getInputStream()));
				isConnected = true;
			} catch (ConnectException e) {
				console.println("Failed to connect. Retrying");
				try {
					Thread.sleep(3000);
				} catch (InterruptedException e1) {
					e1.printStackTrace();
					System.exit(0);
				}
			} catch (Exception e) {
				console.println("Unhandled exception!");
				e.printStackTrace();
				System.exit(0);
			}
		}
		
		console.println("Connected!");
		
		ControllerHandler cont = new ControllerHandler();
		cont.sendControls(objOut, null);
		
	}
}
