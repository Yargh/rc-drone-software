package application;

/**
 * TextField updater class
 * @author Eli Kaplan
 *
 */


import javafx.application.Platform;
import javafx.scene.control.TextField;

public class FieldsUpdater {
	private TextField dirField;
	private TextField dirMagField;
	private TextField elevMagField;
	private TextField rotMagField;
	private TextField curPos;
	private TextField tgtPos;

	private GPSCoord curTgtCoord;
	
    public FieldsUpdater(TextField dir, TextField dirMag, TextField elevMag, TextField rotMag, TextField curPosField, TextField tgtPosField)
    {
    	dirField = dir;
    	dirMagField = dirMag;
    	elevMagField = elevMag;
    	rotMagField = rotMag;
    	curPos = curPosField;
    	tgtPos = tgtPosField;
    	
    	curTgtCoord = new GPSCoord();
    }

    public synchronized void updateFields(DataPacket data)
    {
		Platform.runLater(() ->dirField.setText("" + data.dirDeg));
		Platform.runLater(() ->dirMagField.setText("" + data.moveMag));
		Platform.runLater(() ->elevMagField.setText("" + data.elevMag));
		Platform.runLater(() ->rotMagField.setText("" + data.rotMag));
		
	}
    
    public synchronized void updateGPSFields(GPSCoord cur, GPSCoord tgt)
    {
    	curTgtCoord = tgt;
    	String curTxt, tgtText;
    	curTxt = String.format("%.7f, %.7f", cur.lat, cur.lon);
    	tgtText = String.format("%.7f, %.7f", tgt.lat, tgt.lon);
    	Platform.runLater(() ->curPos.setText(curTxt));
    	Platform.runLater(() ->tgtPos.setText(tgtText));
    }
    
    public synchronized GPSCoord getGPSTarget() {
    	return curTgtCoord;
    }
    
    
}
