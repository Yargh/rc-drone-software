package application;
/**
 * Controller handling class
 * @author Eli Kaplan
 *
 */
import java.io.ObjectOutputStream;
import java.io.ObjectInputStream;
import java.io.PrintStream;
import java.math.BigDecimal;
import java.math.RoundingMode;

import ch.aplu.xboxcontroller.*;


public class ControllerHandler 
{
	private XboxController cont;
	private boolean isConnected = false;
	private boolean quit = false;
	
	PrintStream console;
	FieldsUpdater fields;
	
	GPSCoord tgtCoord;
	
	ReturnDataPacket returnData;
	boolean returnDataUpdated = false;

	private double moveDir = 0.0; // movement direction
	private double moveMag = 0.0; // movement magnitude
	private double elevMag = 0.0; // elevation change magnitude
	private double rotMag = 0.0; // rotation change magnitude
	
	private double lStickDZ = 0.1; // "dead zone" on left analog stick
	
	public ControllerHandler() 
	{
		while(MainController.console == null || MainController.fields == null) {
			try {
				Thread.sleep(1000);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		console = MainController.console;
		fields = MainController.fields;
		tgtCoord = new GPSCoord();
		
		console.println("Initializing controller.");
		//System.load(System.getProperty("user.dir") + "\\lib\\xboxcontroller64.dll");
		cont = new XboxController(System.getProperty("user.dir") + "\\lib\\xboxcontroller64.dll", 1, 50, 50);
		
		
		if(!cont.isConnected()) {
			console.println("No controller found!");
			cont.release();
			return;
		}
		else {
			console.println("Controller connected!");
			isConnected = true;
			cont.addXboxControllerListener(new InternalControllerHandler());
		}
	}
	
	public boolean isConnected()
	{
		return isConnected;
	}
	
	public double getMoveDir()
	{
		return new BigDecimal(moveDir).setScale(2, RoundingMode.HALF_UP).doubleValue(); // round to 2 places
	}
	
	public double getMoveMag()
	{
		return new BigDecimal(moveMag).setScale(2,  RoundingMode.HALF_UP).doubleValue();
	}
	
	public double getElevMag()
	{
		return new BigDecimal(elevMag).setScale(2,  RoundingMode.HALF_UP).doubleValue();
	}
	
	public double getRotMag()
	{
		return new BigDecimal(rotMag).setScale(2,  RoundingMode.HALF_UP).doubleValue();
	}
	
	
	public synchronized void sendControls(ObjectOutputStream objOut, ObjectInputStream objIn)
	{
		if(objOut != null) {
			while(!Thread.interrupted() && !quit) {
				try {
					double txMove = getMoveDir();
					double txMoveMag = getMoveMag();
					double txElevMag = getElevMag();
					double txRotMag = getRotMag();
					
					tgtCoord = fields.getGPSTarget();
					
					if(txMoveMag < lStickDZ) { // we can't do this elsewhere or it breaks
						txMoveMag = 0.0;
					}
		
					DataPacket data = new DataPacket(txMove, txMoveMag, txElevMag, txRotMag, tgtCoord);
					fields.updateFields(data);
					objOut.writeObject(data);
					objOut.flush();
					Thread.sleep(20);
					
					/*
					ReturnDataPacket rd = new ReturnDataPacket (new GPSCoord());
					if(objIn.available() > 0) {
						rd = (ReturnDataPacket) objIn.readObject();
					}
					
					if(rd.curPos.lat != returnData.curPos.lat ||
							rd.curPos.lon != returnData.curPos.lon) { // if updated, do stuff
						returnData.curPos.lat = rd.curPos.lat;
						returnData.curPos.lon = rd.curPos.lon;
						
						fields.updateGPSFields(returnData.curPos, fields.getGPSTarget());
						returnDataUpdated = true;
						
					}
					*/
					
				}
				catch (Exception e) {
					e.printStackTrace();
					Thread.currentThread().interrupt();
				}
			}
			
			if(quit) {
				System.exit(0);
			}
		} else {
			console.println("Null output stream!");
		}
	}
	
	public synchronized ReturnDataPacket getReturnData() {
		returnDataUpdated = false;
		return returnData;
	}
	
	public synchronized boolean isReturnDataUpdated() {
		return returnDataUpdated;
	}
	
	/**
	 * Xbox control handler
	 */
	
	private class InternalControllerHandler extends XboxControllerAdapter
	{
		public InternalControllerHandler()
		{
			
		}
		
		
		public void rightTrigger(double value)
		{
			elevMag = value;
		}
		
		public void leftTrigger(double value)
		{
			elevMag = -value;
		}
		
		public void leftThumbDirection(double direction)
		{
			if(moveMag > lStickDZ) {
				moveDir = direction;
			} else {
				moveDir = 0.0;
			}
		}
		
		public void leftThumbMagnitude(double magnitude)
		{
			moveMag = magnitude;
			
		}
		
		public void leftShoulder(boolean pressed)
		{
			if(pressed) {
				rotMag = -1.0;
			} else {
				rotMag = 0.0;
			}
		}
		
		public void rightShoulder(boolean pressed)
		{
			if(pressed) {
				rotMag = 1.0;
			} else {
				rotMag = 0.0;
			}
		}
		
		public void start(boolean pressed) {
			quit = pressed;
		}
				
		public void isConnected(boolean connected)
		{
			isConnected = connected;
		}
	}
	
	
}
