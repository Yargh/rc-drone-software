package application;

import javafx.scene.control.Button;
import javafx.scene.control.TabPane;

/**
 * JavaFX main window controller
 * @author Eli Kaplan
 *
 */

import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.web.*;

import java.io.PrintStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.*;

public class MainController implements Initializable
{
	@FXML private WebView webPane;
	@FXML private TextArea textOut;
	@FXML private TextField dirField;
	@FXML private TextField dirMagField;
	@FXML private TextField elevMagField;
	@FXML private TextField rotMagField;
	@FXML private TabPane setupDiagTabPane;
	@FXML private TextField ipField;
	@FXML private TextField portField;
	@FXML private Button connectButton;
	@FXML private TextField curPosField;
	@FXML private TextField tgtPosField;
	
	public static TextAreaConsole textAreaOut;
	public static PrintStream console;
	public static FieldsUpdater fields;
	public static GPSCoord curCoord, tgtCoord;
	
	boolean connected = false;
	
	private WebEngine engine;
	
	public MainController() 
	{
		curCoord = new GPSCoord();
		tgtCoord = new GPSCoord();
	}
	
	String initialWebContent()
	{
		String a;
		a = "<!DOCTYPE html>"
		+ "<html>"
		+ "<head>"
		+ "<script src=\"http://maps.googleapis.com/maps/api/js\"></script>"
		+ "<script>"
		+ "var marker = null;"
		+ "var map = null;"
		+ "function initialize() {"
		+ "  var mapProp = {"
		+ "    center:new google.maps.LatLng(45.484461,-122.730216),"
		+ "    zoom:20,"
		+ "    disableDefaultUI: true,"
		+ "    mapTypeId:google.maps.MapTypeId.ROADMAP"
		+ "  };"
		+ "  map=new google.maps.Map(document.getElementById(\"googleMap\"),mapProp);"
		+ "  marker = new google.maps.Marker({"
		+ "    position:map.getCenter(),"
		+ "    map: map,"
		+ "    draggable: true"
		+ "    });"
		+ "  google.maps.event.addListener(map, 'click', function(event) {"
		+ "    updateMarkerPos(marker, event);"
		+ "  });"
		+ "  function updateMarkerPos(marker, e) {"
		+ "    marker.setPosition(e.latLng);"
		+ "  }"
		+ "}"
		+ "google.maps.event.addDomListener(window, 'load', initialize);"
		+ "</script>"
		+ "</head>"
		+ "<body>"
		+ "<div id=\"googleMap\" style=\"width:772px;height:460px;\"></div>"
		+ "</body>"
		+ "</html>";
		
		return a;

	}

	public void mapClickHandler(MouseEvent e) {
		try {
			Thread.sleep(100);
		} catch (Exception e2) {
			// yikes
			
		}
		String curMarkerPos = engine.executeScript("marker.position;").toString();
		curMarkerPos = curMarkerPos.replaceAll("\\(","").replaceAll("\\)", "");
		String[] splitPos = curMarkerPos.split(", ");
		tgtCoord.lat = new Float(splitPos[0]);
		tgtCoord.lon = new Float(splitPos[1]);
		Platform.runLater(() ->fields.updateGPSFields(curCoord, tgtCoord));
	}
	
	@Override
	public void initialize(URL arg0, ResourceBundle arg1)
	{
		if(webPane != null) {
			engine = webPane.getEngine();
			engine.loadContent(initialWebContent());
			
		} else {
			System.out.println("Oy Vey! The WebView isn't populated!");
		}
		
		if(textOut != null) {
			textOut.setEditable(false);
			textOut.setStyle("-fx-focus-color: transparent;");
			
			textAreaOut = new TextAreaConsole(textOut);
			console = new PrintStream(textAreaOut, true);
			
		} else {
			System.out.println("Oy Gevalt! The Text Area isn't populated!");
		}
		
		if(dirField != null && dirMagField != null && elevMagField != null && rotMagField != null && curPosField != null && tgtPosField != null) {
			dirField.setEditable(false);
			dirMagField.setEditable(false);
			elevMagField.setEditable(false);
			rotMagField.setEditable(false);
			fields = new FieldsUpdater(dirField, dirMagField, elevMagField, rotMagField, curPosField, tgtPosField);
			webPane.addEventHandler(MouseEvent.MOUSE_RELEASED, new EventHandler<MouseEvent>() {
				@Override
				public void handle(MouseEvent arg0) {		
					mapClickHandler(arg0);
				}
			});
			curCoord.lat = 45.484461f;
			curCoord.lon = -122.730216f;
			Platform.runLater(() ->fields.updateGPSFields(curCoord, tgtCoord));
		} else {
			System.out.println("Oy!! One of the fields is not populated...");
		}
		
	}
	
	@FXML protected void connectButtonHandler(ActionEvent event)
	{

		
		if(!connected) {
			connected = true;
			RCMain RCController = new RCMain();
			if(!ipField.getText().equals("") && !portField.getText().equals("")) {
				RCController = new RCMain(ipField.getText(), new Integer(portField.getText()));
			}
			
			RCController.start();
		} else {
			console.println("Please relaunch to connect to a different host");
		}
	}
	
	
	
}
