package application;
/**
 * JavaFX Main
 * @author Eli Kaplan
 *
 */


import java.io.BufferedOutputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.stage.Stage;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.fxml.*;


public class Main extends Application 
{
	
	@Override
	public void start(Stage stage) throws Exception
	{
		Parent root = FXMLLoader.load(getClass().getResource("MainView.fxml"));
		Scene scene = new Scene(root);
		
		stage.setTitle("Oy Gevalt!");
		stage.setScene(scene);
		stage.setResizable(false);
		
		stage.show();
		
	}
	
	public static void main(String[] args)
	{
		launch(args);
	}
	
	@Override
	public void stop()
	{
		//System.out.println("Closing application.");
		Platform.exit();
		System.exit(0);
	}
	
}
