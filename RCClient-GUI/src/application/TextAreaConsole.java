package application;

/**
 * Text Area Console
 * Thanks to Dreen on StackOverflow
 * @author Eli Kaplan
 *
 */

import java.io.IOException;
import java.io.OutputStream;

import javafx.application.Platform;
import javafx.scene.control.TextArea;

public class TextAreaConsole extends OutputStream{
	 private TextArea output;

    public TextAreaConsole(TextArea ta)
    {
        output = ta;
    }

    @Override
    public void write(int i) throws IOException
    {
        Platform.runLater(() -> output.appendText(String.valueOf((char) i)));
    }
}
