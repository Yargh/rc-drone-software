package application;
import java.io.Serializable;

public class GPSCoord implements Serializable {
	public float lat;
	public float lon;
	
	private static final long serialVersionUID = 1L;
	
	public GPSCoord(float lt, float ln)
	{
		lat = lt;
		lon = ln;
	}
	
	public GPSCoord()
	{
		lat = 0.0f;
		lon = 0.0f;
	}
}
