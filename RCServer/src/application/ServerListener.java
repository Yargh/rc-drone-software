package application;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;

import javax.net.ServerSocketFactory;

/**
 * Server listener thread
 * @author Eli Kaplan
 *
 */
public final class ServerListener extends Thread 
{
	private ServerSocket socket;
	
	private DataPacket curData;
	
	Thread parentThread;
	
	public ServerListener(int port, Thread parent) throws IOException
	{
		socket = ServerSocketFactory.getDefault().createServerSocket(port);
		socket.setReuseAddress(true);
		curData = new DataPacket(0.0, 0.0, 0.0, 0.0);
		parentThread = parent;
	}
	
	@Override
	public void run() {
		while(!Thread.currentThread().isInterrupted()) {
			try {
				Socket socketToClient = socket.accept();
				ServerClientHandler clientHandler = new ServerClientHandler(socketToClient, Thread.currentThread());
				clientHandler.start();
				while(!clientHandler.isInterrupted()) {
					setCurData(clientHandler.getCurData());
					try { Thread.sleep(5); } catch (Exception e) {
						Thread.currentThread().interrupt();
					}
				}
				
				Thread.currentThread().interrupt();
				
				
			} catch (Exception e) {
				e.printStackTrace();
				Thread.currentThread().interrupt();
			}
		}
		
		parentThread.interrupt();
	}
	
	public synchronized DataPacket getCurData()
	{
		if(curData != null) {
			return new DataPacket(curData.dirDeg, curData.moveMag, curData.elevMag, curData.rotMag, curData.gpsCoord);
		}
		else {
			return new DataPacket(0.0, 0.0, 0.0, 0.0);
		}
		
	}
	
	public synchronized void setCurData(DataPacket d)
	{
		curData = d;
	}
	
}


final class ServerClientHandler extends Thread
{
	private Socket socket;
	ObjectInputStream objInput;
	DataPacket outData;
	Thread parentThread;
	DataOutput output;
	
	//ObjectOutputStream objOutput;
	
	public ServerClientHandler(Socket s, Thread parent) throws IOException
	{
		socket = s;
		objInput = new ObjectInputStream(new BufferedInputStream(socket.getInputStream()));
		//objOutput = new ObjectOutputStream(new BufferedOutputStream(socket.getOutputStream()));
		parentThread = parent;
		output = new DataOutput();
		outData = new DataPacket(0, 0, 0, 0);
	}
	
	@Override
	public void run()
	{
		//System.out.println("Client connected: " + socket.getRemoteSocketAddress());
		
		while(!Thread.currentThread().isInterrupted()) {
			try {
				DataPacket d = (DataPacket) objInput.readObject();
				// only update the output if the values have changed at all - save bandwidth?
				if(d.dirDeg != outData.dirDeg || 
						d.elevMag != outData.elevMag ||
						d.moveMag != outData.moveMag ||
						d.rotMag != outData.rotMag || 
						d.gpsCoord.lat != outData.gpsCoord.lat ||
						d.gpsCoord.lon != outData.gpsCoord.lon) {
					
					outData = d;
					output.updateData(outData);
				}
				
				output.outputData();
				
				/*
				if(output.isReturnUpdated()) {
					objOutput.writeObject(output.getReturnData());
					objOutput.flush();
				}
				*/
				
			} catch (SocketException se) {
				//System.out.println("\nSocket exception. Probably closed. Resetting.");
				
				if(parentThread != null) {
					parentThread.interrupt();
				}
				
				try {
					Thread.sleep(10); // Prevent the apocalypse
				} catch (InterruptedException e) {
					// We failed to prevent it. ripperoni
					e.printStackTrace();
				}
				
				Thread.currentThread().interrupt();
			} catch (Exception e) {
				e.printStackTrace();
				Thread.currentThread().interrupt();
			}
		}
	}
	
	public synchronized DataPacket getCurData()
	{
		return outData;
	}
}
