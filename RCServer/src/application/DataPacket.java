package application;

import java.io.Serializable;

/**
 * Data packet passed between server and client
 * @author Eli Kaplan
 * 
 */
public class DataPacket implements Serializable
{
	private static final long serialVersionUID = 1L;
	
	public double dirDeg;
	public double moveMag;
	public double elevMag;
	public double rotMag;
	
	public GPSCoord gpsCoord; 
	
	public DataPacket(double dir, double move, double elev, double rot)
	{
		dirDeg = dir;
		moveMag = move;
		elevMag = elev;
		rotMag = rot;
		gpsCoord = new GPSCoord();
	}
	
	public DataPacket(double dir, double move, double elev, double rot, GPSCoord gps) {
		dirDeg = dir;
		moveMag = move;
		elevMag = elev;
		rotMag = rot;
		gpsCoord = new GPSCoord();
		gpsCoord.lat = gps.lat;
		gpsCoord.lon = gps.lon;
	}
	
	@Override
	public String toString() {
		return ""
				+ dirDeg
				+ " "
				+ moveMag
				+ " "
				+ elevMag
				+ " " 
				+ rotMag
				+ " "
				+ gpsCoord.lat
				+ " "
				+ gpsCoord.lon;
	}
}