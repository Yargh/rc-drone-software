package application;
/**
 * Server for RC quad over Internet
 * @author Eli Kaplan
 *
 */

public class Main 
{
	static int serverPort = 6969; // port to listen on
	static ServerListener server = null;
	
	public static void main(String[] args) throws InterruptedException 
	{
		//System.out.println("Starting RC server...");
		serverLoop();
		Thread.sleep(1000);
		System.exit(0);
	}
	
	public static void serverLoop() {
		try {
			server = new ServerListener(serverPort, Thread.currentThread());
			server.start();
			
			//System.load(System.getProperty("user.dir") + "\\rxtxSerial.dll");
			//System.load(System.getProperty("user.dir") + "/librxtxSerial.so");
			//System.out.println("Listening on port " + serverPort + " .");
			
		} catch (Exception e) {
			e.printStackTrace();
			System.exit(0);
		}
		
		if(server != null) {
			//ArduinoSerial serial = new ArduinoSerial();
			//serial.init();
			
			while(!server.isInterrupted()) {
				try { Thread.sleep(5); } catch (Exception e) {
					//serial.close();
					//System.exit(0);
					//System.out.println("Server interrupted.");
				}
				//serial.setOutputPacket(server.getCurData());
			}
			//serial.close();
			//System.exit(0);
		}
	}

}
