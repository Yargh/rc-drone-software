package application;

public class DataOutput {
	DataPacket data;
	ReturnDataPacket returnData;
	boolean dataRefreshed = false;
	boolean returnDataRefreshed = false;
	
	public DataOutput() {
		data = new DataPacket(0,0,0,0);
		returnData = new ReturnDataPacket(new GPSCoord());
		
		dataRefreshed = false;
		returnDataRefreshed = false;
	}
	
	public synchronized void updateData(DataPacket d) {
		data = d;
		dataRefreshed = true;
	}
	
	public synchronized void outputData() {
		if(dataRefreshed) {
			DataPacket outData = data;
			//System.out.print(outData.toString() + "\n");
			
			//System.out.println("direction " + (int) outData.dirDeg);
			//System.out.println("speed " + (int) (100.0f * outData.moveMag));
			System.out.println("throttle " + (int) (100.0f * outData.elevMag));
			//System.out.println("rotation " + (int) (100.0f * outData.rotMag));
			
			
			dataRefreshed = false;
			
		}
	}
	
	
	
	/*
	public synchronized boolean isReturnUpdated() {
		return returnDataRefreshed;
	}
	
	public synchronized ReturnDataPacket getReturnData() {
		return returnData;
	}
	*/
	
}
